import java.util.*;
import java.util.Arrays;

public class Vigenere {
  
  
  
  public static String decrypt(String key, String cipher) {
    
    // Initialize varibles and arrays
    int keyLength = key.length();
    int cipherLength = cipher.length();
    char[] keyChars = new char[keyLength];
    int[]  keyAscii = new int[keyLength];
    char[] cipherChars = new char[cipherLength];
    int[]  cipherAscii = new int[cipherLength];
    String[] clearString = new String[cipherLength];
    int[]  clearAscii = new int[cipherLength];;
    String clear;
    
    // Store the key as an array of chars and store the Ascii values
    for(int i = 0; i < key.length(); i++) {
      keyChars[i] = key.charAt(i);
      keyAscii[i] = (int) keyChars[i];
    }
    
    // Store the cipher as an array of chars and store the Ascii values
    cipher.toUpperCase();
    for (int i = 0; i < cipher.length(); i++) {
      cipherChars[i] = cipher.charAt(i);
      cipherAscii[i] = (int) cipherChars[i];
    }
    // Create ciphertext
    int j = 0;
    for (int i = 0; i < cipher.length(); i++) {
      clearAscii[i] = cipherAscii[i] - (keyAscii[j] -65);
      if (clearAscii[i] < 65) clearAscii[i] = clearAscii[i] + 26;
      clearString[i] = Character.toString ((char) clearAscii[i]);
      j++;
      if (j == key.length()) j = 0;
    }
    
    // Link the strings from clearString[] into one big string
    clear = Arrays.toString(clearString);
    
    return clear;
  }
  
  public static void crack(int keyspace, String cipher) {
    // Initialize
    String key = "";
    String clear;
    int[] keyAscii = new int[keyspace];
    Arrays.fill(keyAscii, 65);
    double iters = (Math.pow(26.0, (double) keyspace));
    
    // Form string from array of Ascii values
    StringBuilder sb = new StringBuilder();
    for (int j = 0; j < keyspace; j++) {
      sb.append( (char) keyAscii[j] );
    }
    key = sb.toString();
    
    // Try every possible key
    for (int i = 0; i < iters; i++) {
      
      // Decrypt that key
      clear = decrypt(key, cipher);
      
      // Print
      System.out.println(key + ": " + clear);
      
      // Get next key
      key = nextKey(key);
    }
  }
  
  public static String nextKey(String key) {
    int keyspace = key.length();
    StringBuilder sb = new StringBuilder(key);
    if ( (int) key.charAt(keyspace - 1) == 90 ) {
      for (int i = 1; i < keyspace; i++) {
        if ( (int) key.charAt(keyspace - i) == 90 ) {
          sb.setCharAt(keyspace - i, 'A');
          int current = (int) sb.charAt(keyspace - (i + 1));
          char next = (char) (current + 1);
          sb.setCharAt(keyspace - (i + 1), next);
        }
      }
    key = sb.toString();
    return key;
    }
    else {
      int current = (int) sb.charAt(keyspace - 1);
      char next = (char) (current + 1);
      sb.setCharAt(keyspace - 1, next);
      key = sb.toString();
      return key;
    }
  }
  
  public static void main(String args[]) {
    
    // Initialize values
    String cipher = "mwcy wbxs wi fvtlwig ntl rzceexhdnt zpwn mryaobe. luc vvvr iwaklrzmr ntrv bkj os\r\n" + 
    		"zpwn afyquimrtb. hj cbsxzztr zps aiagt goec un hce nyawbnzkvh, toh scgo shhuwo\r\n" + 
    		"tb g jwobhisso rrvwgdtbxg hce puls (dn ntg zvntaiuz) fbx bvz tbutg ohnz gcp ufkl\r\n" + 
    		"hj drizmkt gnqg hefyiuz aaj bvz pekdwjuf xiwg frtks hefyiuz. cekihz a cxqjvtr\r\n" + 
    		"hqhwupqmh mecuawooee voheq ia1653-jdgrtmfz-aoi123, evzrr gjq123 ds lucf kigz\r\n" + 
    		"cgzragus, oo uutr tohx aiwmvyawjn.";
    int keyspace = 6;
    cipher = cipher.replaceAll("[^a-z]", "");
    cipher = cipher.replaceAll("\\s+","");
    cipher = cipher.toUpperCase();
    crack(keyspace, cipher);
  }
}